package com.example.tinkofffintechexample

import com.google.gson.annotations.SerializedName

class VkUser(

    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("photo_200")
    val photo: String
)
