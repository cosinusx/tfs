package com.example.tinkofffintechexample

import retrofit2.Call
import retrofit2.http.GET

interface VkService {

    @GET("users.get?fields=photo_200")
    fun getUserInfo(): Call<ApiResponse>
}
