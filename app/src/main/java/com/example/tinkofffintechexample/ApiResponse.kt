package com.example.tinkofffintechexample

import com.google.gson.annotations.SerializedName

class ApiResponse(

    @SerializedName("response")
    val response: List<VkUser>
)
