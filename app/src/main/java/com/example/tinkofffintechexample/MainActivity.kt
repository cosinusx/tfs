package com.example.tinkofffintechexample

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {

    private lateinit var retrofit: Retrofit
    private lateinit var vkService: VkService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginButton.setOnClickListener {
            VK.login(this, emptyList())
        }

        getUserButton.setOnClickListener {
            vkService.getUserInfo().enqueue(object : Callback<ApiResponse> {
                override fun onFailure(call: Call<ApiResponse>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "Get profile info error", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(call: Call<ApiResponse>, response: Response<ApiResponse>) {
                    response.body()?.response?.firstOrNull()?.let { user ->
                        showUser(user)
                    }
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                Toast.makeText(this@MainActivity, "Auth success", Toast.LENGTH_SHORT).show()
                createRetrofit(token.accessToken)
                createVkService()
                getUserButton.isEnabled = true
            }

            override fun onLoginFailed(errorCode: Int) {
                Toast.makeText(this@MainActivity, "Auth failed", Toast.LENGTH_SHORT).show()
            }
        }
        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun createRetrofit(token: String) {
        val client = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val urlBuilder = chain.request().url.newBuilder()
                    .addQueryParameter("access_token", token)
                    .addQueryParameter("v", "5.122")

                chain.proceed(
                    chain.request().newBuilder()
                        .url(urlBuilder.build())
                        .build()
                )
            }
            .build()

        retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl("https://api.vk.com/method/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createVkService() {
        vkService = retrofit.create(VkService::class.java)
    }

    @SuppressLint("SetTextI18n")
    private fun showUser(user: VkUser) {
        Glide.with(this)
            .load(user.photo)
            .into(photoView)

        nameView.text = "${user.firstName} ${user.lastName}"
    }
}
